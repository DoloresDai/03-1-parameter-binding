package com.twuc.webApp.com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void bind_variable_to_Integer() throws Exception {
        mockMvc.perform(get("/api/users/9"))
                .andExpect(status().isOk())
                .andExpect(content().string("9"));
    }

    @Test
    void bind_variable_to_int() throws Exception {
        mockMvc.perform(get("/api/students/100"))
                .andExpect(status().isOk())
                .andExpect(content().string("100"));
    }

    @Test
    void bind_two_variable_and_success() throws Exception {
        mockMvc.perform(get("/api/users/9/books/2"))
                .andExpect(status().isOk())
                .andExpect(content().string("11"));
    }

    @Test
    void should_return_user_information_when_give_username_and_age() throws Exception {
        mockMvc.perform(get("/api/user")
                .param("name", "lili")
                .param("age", "9"))
                .andExpect(status().isOk())
                .andExpect(content().string("my name is lili,I am 9 years old"));
    }

    @Test
    void should_return_default_value() throws Exception {
        mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andExpect(content().string("my name is lili2,I am 0 years old"));
    }

    @Test
    void should_return_collection_when_bind_collection() throws Exception {
        mockMvc.perform(get("/api/collections")
                .param("id", "1,2,3", "4"))
                .andExpect(status().isOk())
                .andExpect(content().string("[1,2,3, 4]"));
    }

    @Test
    void should_return_json_when_serialize() throws Exception {
        mockMvc.perform(post("/api/contract/serialize")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"name\":\"lili\",\"age\":19}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("lili"))
                .andExpect(jsonPath("$.age").value("19"));
    }

    @Test
    void should_return_json_when_deserialize() throws Exception {
        mockMvc.perform(get("/api/contract/deserialize")
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .param("json","{\"name\":\"dai\",\"age\":19}"))
                .andExpect(jsonPath("$.name").value("dai"))
                .andExpect(jsonPath("$.age").value(19));
    }

    @Test
    void should_return_local_datetime() throws Exception {
        mockMvc.perform(post("/api/datetime")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }"))
                .andExpect(status().isOk());
    }


}
