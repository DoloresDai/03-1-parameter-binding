package com.twuc.webApp.com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.com.twuc.webApp.model.Contract;
import com.twuc.webApp.com.twuc.webApp.model.LocalDataTime;
import com.twuc.webApp.com.twuc.webApp.model.Person;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;

@RestController
public class UserController {
    private String name;

    @GetMapping("/api/users/{id}")
    String bindVariableToInteger(@PathVariable Integer id) {
        return id.toString();
    }

    @GetMapping("/api/students/{id}")
    String bindVariableToInt(@PathVariable int id) {
        return String.valueOf(id);
    }

    @GetMapping("/api/users/{userId}/books/{bookId}")
    String bindTwoVariable(@PathVariable() Integer userId, @PathVariable Integer bookId) {
        return String.valueOf(userId + bookId);
    }

    @GetMapping("/api/user")
    String bindVariable(@RequestParam(value = "name", defaultValue = "lili2") String name, @RequestParam(value = "age", defaultValue = "0") int age) {
        return new Person(name, age).introduce();
    }

    @GetMapping("/api/collections")
    String returnCollection(@RequestParam("id") List<String> list) {
        return list.toString();
    }

    @PostMapping("/api/contract/serialize")
    String jsonSerialize(@RequestBody Contract contract) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(contract);
    }

    @GetMapping("/api/contract/deserialize")
    public Contract jsonDeserialize(@RequestParam String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Contract.class);
    }

    @PostMapping("/api/datetime")
    public LocalDataTime deserializeTime(@RequestBody LocalDataTime dataTime) {
        return dataTime;
    }
}
