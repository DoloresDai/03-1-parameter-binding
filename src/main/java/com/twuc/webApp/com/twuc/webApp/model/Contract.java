package com.twuc.webApp.com.twuc.webApp.model;

public class Contract {
    private String name;
    private Integer age;

    public Contract() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer year) {
        this.age = year;
    }
}
