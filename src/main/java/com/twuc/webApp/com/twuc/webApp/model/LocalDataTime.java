package com.twuc.webApp.com.twuc.webApp.model;

import java.time.ZonedDateTime;

public class LocalDataTime {
    private ZonedDateTime dateTime;

    public ZonedDateTime getDateTime() {
        return dateTime;
    }
}
