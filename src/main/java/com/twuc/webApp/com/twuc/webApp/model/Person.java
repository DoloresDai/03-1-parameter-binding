package com.twuc.webApp.com.twuc.webApp.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Person {
    @NotNull
    private String name;
    @Max(100)
    @Min(0)
    private int age;

    @Email
    private String email;

    public Person(@NotNull String name, @Max(100) @Min(0) int age, @Email String email) {
        this.name = name;
        this.age = age;
        this.email = email;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String introduce() {
        return "my name is " + this.name + ",I am " + this.age + " years old";
    }
}
